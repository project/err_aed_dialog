<?php

namespace Drupal\err_aed_dialog\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsFormatterBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @FieldFormatter(
 *   id = "err_aed_dialog",
 *   label = @Translation("ERR Display Dialog widget"),
 *   field_types = {
 *     "entity_reference_revisions",
 *   },
 * )
 */
class EntityReferenceDisplayDialogFormatter extends EntityReferenceRevisionsFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LoggerChannelFactoryInterface $logger_factory, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->loggerFactory = $logger_factory;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $plugin_id,
        $plugin_definition,
        $configuration['field_definition'],
        $configuration['settings'],
        $configuration['label'],
        $configuration['view_mode'],
        $configuration['third_party_settings'],
        $container->get('logger.factory'),
        $container->get('entity_display.repository')
        );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'use_entity_label' => TRUE,
        'link_title' => '',
        'dialog_width' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element = parent::settingsForm($form, $form_state);

    $element['use_entity_label'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Use entity label'),
        '#description' => $this->t('Uses the entity label as the link title instead of the text specified.'),
        '#default_value' => $this->getSetting('use_entity_label'),
    ];

    $element['link_title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Link title'),
        '#description' => $this->t('The link title to use if not using the entity label.'),
        '#default_value' => $this->getSetting('link_title'),
    ];

    $element['dialog_width'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Dialog width'),
        '#description' => $this->t('Enter a width value, or leave blank for automatic width.'),
        '#default_value' => $this->getSetting('dialog_width'),
    ];

    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Renders the field as a Dialog link.');

    $useEntityLabel = $this->getSetting('use_entity_label');

    $summary[] = $useEntityLabel ? 'Using the entity label' : 'Using the specified title';

    if (!$useEntityLabel) {
      $summary[] = $this->t('Link title: ') . $this->getSetting('link_title');
    }

    $width = $this->getSetting('dialog_width');
    if (!$width) {
      $width = 'auto';
    }

    $summary[] = $this->t("Dialog width: $width");

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $element = array();

    $element = [
        '#attached' => [
            'library' => [
                'err_aed_dialog/add_edit_dialog',
            ],
        ]
    ];

    $class = 'entity-reference-dialog';
    if (isset($items[0]->entity)) {
      /** @var EntityInterface $entity */
      $entity = $items[0]->entity;

      $class .= ' entity-reference-dialog--' . $entity->getEntityTypeId();
    }

    $dialogOptions = [
        'dialogClass' => $class
    ];
    $width = $this->getSetting('dialog_width');

    if ($width) {
      $dialogOptions['width'] = $width;
    }

    /** @var EntityReferenceItem $item */
    foreach ($items as $item) {
      if (!empty($item->target_id) && isset($item->entity)) {
        /** @var EntityInterface $entity */
        $entity = $item->entity;
        $entity = $entity->getTranslation(\Drupal::languageManager()->getCurrentLanguage()->getId());
        $element[] = [
            '#type' => 'link',
            '#attributes' => [
                'class' => 'use-ajax',
                'data-dialog-type' => 'modal',
                'data-dialog-options' => json_encode($dialogOptions),
            ],
            '#title' => $this->getTitle($entity),
            '#url' => $entity->toUrl('canonical'),
        ];
      }
    }

    return $element;

  }

  /**
   * Get the correct title value to use for this reference.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The title.
   */
  protected function getTitle(EntityInterface $entity) {
    if ($this->getSetting('use_entity_label')) {
      return $entity->label();
    }

    return $this->getSetting('link_title');
  }

  /**
   * {@inheritdoc}
   */
//   public static function isApplicable(FieldDefinitionInterface $field_definition) {
//     // This formatter is only available for entity types that have a view
//     // builder.
//     $target_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
//     return \Drupal::entityTypeManager()->getDefinition($target_type)->hasViewBuilderClass();
//   }

}
