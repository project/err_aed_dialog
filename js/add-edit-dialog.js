(function ( $, Drupal, drupalSettings ) {
    Drupal.behaviors.add_edit_dialog = {
        attach: function(context, settings) {
        	// to open modal on another modal.
        	if( $("#" + 'drupal-modal-1').length == 0 ) {
        		$('#drupal-modal').attr('id', 'drupal-modal-1');
        	} else if( $("#" + 'drupal-modal-2').length == 0 ) {
        		$('#drupal-modal').attr('id', 'drupal-modal-2');
        	} else if( $("#" + 'drupal-modal-3').length == 0 ) {
        		$('#drupal-modal').attr('id', 'drupal-modal-3');
	        } else if( $("#" + 'drupal-modal-4').length == 0 ) {
	    		$('#drupal-modal').attr('id', 'drupal-modal-4');
	    	}
        }
    };
}(jQuery, Drupal, drupalSettings));